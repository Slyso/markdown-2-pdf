#!/bin/sh

# For specific files:
# markdown_files="title-page.md main.md"

# All *.md files, but ignore hidden files (prefixed with ".")
markdown_files=$(find /data/ -type f \( -name '*.md' ! -name '.*' \) | sort)

bib_file=$(find /data/ -type f -name '*.bib' | head -n 1)

if [ -n "$bib_file" ]; then
  citation="--citeproc --bibliography $bib_file"
else
  citation=""
fi

pandoc --template "/eisvogel/eisvogel.tex" \
  --pdf-engine=xelatex \
  --listings --standalone \
  --number-sections \
  --filter pandoc-plantuml \
  --lua-filter /number-depth.lua \
  $citation \
  $markdown_files -o doc.pdf
